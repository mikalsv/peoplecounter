package peoplecounter.uia.io.peoplecounter.openCV;


import android.graphics.RectF;
import android.util.Log;

public class CounterLine {
   public float x1;
   public float x2;
   public float yTop;
   public float yBot;
   public float delta;

    private final static String TAG="CounterLine";

    public CounterLine(float x1,float x2, float y, float delta){
        this.x1=x1; this.x2=x2;

        yTop=y-delta/2;
        yBot=y+delta/2;

        this.delta=delta;
    }

    public boolean interceptingTop(RectF rect){
     //   Log.e(TAG,"");
        //if the rectangles top is higher than the top line,
        // but its bottom is beneath it, they are intercepting
        if (rect.top<yTop && rect.bottom>yTop)return true;
        else return false;
    }

    public boolean interceptingBot(RectF rect){
        if (rect.top<yBot && rect.bottom>yBot)return true;
        else return false;
    }
}
