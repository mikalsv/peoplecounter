package peoplecounter.uia.io.peoplecounter;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian on 26.02.2015.
 */

    // Object with queryURL, and coords in. Object with eta and distance out.
    public class GcmRegister extends AsyncTask<Void, Void, List<String>> {

        private static final String TAG = "[HELPER] GcmRegister - ";
        public static final String PROPERTY_REG_ID = "registration_id";
        public static final String PROPERTY_DEV_ID = "device_id";

        private static final String PROPERTY_APP_VERSION = "appVersion";
        private Context mContext;

        public GcmRegister (Context context) {
        mContext = context;
        }

        protected List<String>   doInBackground(Void...params) {
            GoogleCloudMessaging gcm;
            String SENDER_ID = "422145115692";
            String REGID = "";
            gcm = GoogleCloudMessaging.getInstance(mContext);

            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(mContext);
            }
            try {
                REGID = gcm.register(SENDER_ID);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Save regid to remote.
            Log.e(TAG, "Your GCM ID - is : " + REGID);

            List<String> rt = new ArrayList<String>();
            rt.add(REGID);

            return rt;

        }

        protected void onPostExecute(List<String> result) {
            String id = result.get(0);

            // For this demo: we don't need to send it because the device
            // will send upstream messages to a server that echo back the
            // message using the 'from' address in the message.

            // Persist the registration ID - no need to register again.
                storeRegistrationId(mContext, id);

        }



    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        Log.i(TAG, "GcmID " + regId);



        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you.
        return mContext.getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
}
