package peoplecounter.uia.io.peoplecounter.openCV;


public class PendingBlob {
    final public static boolean ENTERING_TOP=true;
    final public static boolean ENTERING_BOTTOM=false;

   public float width;
   public float x1;
   public float x2;
   public boolean entering;
    public boolean exiting=false;
    public boolean exists=true;
    public int timeoutFrames=0;

    public PendingBlob(float x1, float x2,boolean entering){
        width=x2-x1;
        this.x1=x1;
        this.x2=x2;
        this.entering=entering;
    }

    public void setPosition(float x1, float x2){
        width=x2-x1;
        this.x1=x1;
        this.x2=x2;
    }

    public void setTracked(){
        this.exists=true;
    }

    public void evaluate(){
           exists=false;

    }

    public void markMissing(){
        timeoutFrames++;
    }
}
