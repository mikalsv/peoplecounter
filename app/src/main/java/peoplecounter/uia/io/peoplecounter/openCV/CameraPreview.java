package peoplecounter.uia.io.peoplecounter.openCV;
import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.io.IOException;
import java.util.List;

/** A basic Camera preview class */

//TODO: fix autofocus
//TODO: fix unfixable aspect ratio
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private final static String TAG="CameraPreview";
    private SurfaceHolder mHolder;
    private Camera mCamera;
   private int screenWidth;
    private int screenHeight;
    private double TARGET_RATIO=1.4;

    public CameraPreview(Context context, Camera camera, int w, int h) {
        super(context);
        mCamera = camera;
        this.screenWidth=w;
        this.screenHeight=h;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.

        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }


    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        Camera.Parameters cparams =mCamera.getParameters();
        Camera.Size size=getSmallestPreviewSize(mCamera);
        cparams.setPreviewSize(size.width,size.height);

       // mCamera.setParameters(cparams);

         ViewGroup.LayoutParams params = getLayoutParams();
        params.height=screenHeight;
        params.width=screenWidth;
        setLayoutParams(params);

     //   Log.e(TAG, "canvas size: " + getWidth() + " x " + getHeight());
        // start preview with new settings
        try {

            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();


        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    private Camera.Size getOptimalCamSize(Camera camera){
      List<Camera.Size> sizes=  camera.getParameters().getSupportedPreviewSizes();
        Camera.Size optimalSize=null;

        for  (Camera.Size size : sizes){
            if (optimalSize==null)optimalSize=size;
            else{
                if (areaOf(size)>areaOf(optimalSize) && (size.height>screenHeight) ){
                    optimalSize=size;
                }
            }
        }
   //     Log.e(TAG,"Returned: " + optimalSize.width + " x " + optimalSize.height);
        return optimalSize;
    }

    private Camera.Size getOptimalCamSizeRatio(Camera camera){
        List<Camera.Size> sizes=  camera.getParameters().getSupportedPreviewSizes();
        Camera.Size optimalSize=null;
        double currentDelta=0;

        for  (Camera.Size size : sizes){
            if (optimalSize==null){optimalSize=size;currentDelta=deltaTarget(size);}
            else{
                if (deltaTarget(size)<currentDelta ) {
                    optimalSize=size;
                }
                else if (deltaTarget(size)==currentDelta){
                    if (areaOf(size)>areaOf(optimalSize))optimalSize=size;
                }
            }
        }
     //   Log.e(TAG,"Returned: " + optimalSize.width + " x " + optimalSize.height);
        return optimalSize;
    }

    private double deltaTarget(Camera.Size size){
        double delta = Math.abs(ratioOf(size)-TARGET_RATIO);
   //     Log.e(TAG,"delta: " + delta);

        return delta;
    }

    private static Camera.Size getSmallestPreviewSize(Camera camera){
        List<Camera.Size> sizes=  camera.getParameters().getSupportedPreviewSizes();
        Camera.Size smallest = null;
        for (Camera.Size size : sizes){
            if (smallest==null)smallest=size;
            else {
                if (size.height*size.width < smallest.height*smallest.width)smallest=size;
            }
        }
        Log.e(TAG,"Returned: " + smallest.width + " x " + smallest.height);
        return smallest;
    }

    private double ratioOf(Camera.Size size){
        return size.width/size.height;
    }
    private int areaOf(Camera.Size size){
        return size.height*size.width;
    }



}