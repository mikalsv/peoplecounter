package peoplecounter.uia.io.peoplecounter;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.File;

public class SFTPConnection extends AsyncTask<Void,Void,Void>
{

    public static String HOST_ADDRESS = "uia.io";
    @Override
    protected Void doInBackground(Void... params) {


        // TODO Auto-generated method stub
        boolean conStatus = false;
        Session session = null;
        Channel channel = null;
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");

        Log.i("Session", "is" + conStatus);
        try {
            JSch ssh = new JSch();
            session = ssh.getSession("pcount", HOST_ADDRESS, 22);
            session.setPassword("counter");
            session.setConfig(config);
            session.connect();
            conStatus = session.isConnected();
            Log.i("Session","is"+conStatus);
            channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftp = (ChannelSftp) channel;
            sftp.put(Environment.getDataDirectory() + File.separator + "test.png", "/peoplecounter/public/img");
        } catch (JSchException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.i("Session","is"+conStatus);
        } catch (SftpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.i("Session","is"+conStatus);
        }
        return null;
    }

}
