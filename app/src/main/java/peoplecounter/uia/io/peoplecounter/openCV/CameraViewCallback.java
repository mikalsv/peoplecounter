package peoplecounter.uia.io.peoplecounter.openCV;

import android.view.View;

public interface CameraViewCallback {

    public void onReceive(View view);
}
