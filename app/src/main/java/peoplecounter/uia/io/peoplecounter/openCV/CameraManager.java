package peoplecounter.uia.io.peoplecounter.openCV;


import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class CameraManager {
    Camera c = null;



   public CameraManager(){

    }

private final static String TAG="CameraManager";

    /** A safe way to get an instance of the Camera object. */
    public  Camera getCameraInstance(){
        if (c!=null)return c;
        try {
            //TODO: use camera ID
            c = Camera.open(); // attempt to get a Camera instance

        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Log.e(TAG, "Camera is not available");
        }
        return c; // returns null if camera is unavailable
    }

    public  void releaseCamera(){
        if (c!=null)c.release();
    }





    private static Camera.Size getSmallestPreviewSize(Camera camera){
     List<Camera.Size> sizes=  camera.getParameters().getSupportedPreviewSizes();
        Camera.Size smallest = null;
        for (Camera.Size size : sizes){
            if (smallest==null)smallest=size;
            else {
                if (size.height*size.width < smallest.height*smallest.width)smallest=size;
            }
        }
        return smallest;
    }
    private static Camera.Size getSmallestWidescreen(Camera camera){
        List<Camera.Size> sizes=  camera.getParameters().getSupportedPreviewSizes();
        Camera.Size smallest = null;
        for (Camera.Size size : sizes){
            if (smallest==null && isWidescreen(size))smallest=size;
            else {
                if (size.height*size.width < smallest.height*smallest.width && isWidescreen(size))smallest=size;
            }
        }
        return smallest;
    }
    private static boolean isWidescreen(Camera.Size size){
        double aspect = (double)size.width/(double)size.height;
        double target = 16.0/9.0;
        Log.e(TAG,"aspect: " + aspect + " target: " + target);
        return (aspect==target);
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
