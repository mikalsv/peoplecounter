package peoplecounter.uia.io.peoplecounter.openCV;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.util.Log;
import android.view.View;


import org.bytedeco.javacpp.FloatPointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * processes images and draws tracking overlay onto camera view
 */
public class OpenCVTesting extends View implements Camera.PreviewCallback {

    private static final String TAG = "OPENCV";

    private int cWidth;
    private int cHeight;
    private static final boolean debug=true;
    private int downSample=8;
    private final static int hz=10;
    private boolean processing=false;

 opencv_core.IplImage image;
    opencv_core.Rect rects;
    opencv_objdetect.HOGDescriptor hog;



    public OpenCVTesting (Context context, Camera camera){
        super(context);
        camera.setPreviewCallback(this);
// Preload the opencv_objdetect module to work around a known bug.
        Loader.load(opencv_objdetect.class);
       Camera.Size prsize=camera.getParameters().getPreviewSize();
        //TODO: check into depth and channels
        cWidth=prsize.width/downSample;
        cHeight=prsize.height/downSample;
image= opencv_core.IplImage.create(cWidth,cHeight,opencv_core.IPL_DEPTH_8U,1);
        hog = new opencv_objdetect.HOGDescriptor();
         hog.svmDetector(opencv_objdetect.HOGDescriptor.getDefaultPeopleDetector());


    }

    @Override
    public void onPreviewFrame(final byte[] data,final Camera camera) {
        if (processing)return;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                processing=true;
                long time1=System.currentTimeMillis();
                Camera.Size size = camera.getParameters().getPreviewSize();
                processImage(data, size.width, size.height);
                camera.addCallbackBuffer(data);
                long time2=System.currentTimeMillis();
                long delta=time2-time1;
                if (delta>1000/hz){
                    processing=false;
                    return;
                }
                else {try {
                    Thread.sleep(delta-(1000/hz));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                processing=false;}
                }


            }
        });
        thread.start();

    }

    private void processImage(byte[] data, int width, int height){
       opencv_core.Rect foundRects = new opencv_core.Rect();
        ByteBuffer imgbuffer =image.getByteBuffer();

        for (int y = 0;y<image.height();y++){
            for (int x = 0;x<image.width();x++){
                imgbuffer.put(x+y*image.widthStep(),data[(y*width*downSample)+downSample*x]);
            }
        }
        opencv_core.Mat mat =new opencv_core.Mat(image);
        hog.detectMultiScale(mat, foundRects, 0, new opencv_core.Size(8, 8), new opencv_core.Size(32, 32), 1.05, 0, false);

        this.setRects(foundRects);
        postInvalidate();
    }

    private synchronized void setRects(opencv_core.Rect rects){
        this.rects=rects;
    }

    private synchronized ArrayList<RectF> getRects(){
        if (this.rects==null)return null;
        ArrayList<RectF> rects = new ArrayList<>();
        for (int i=0; i<this.rects.capacity();i++){
            RectF rect = new RectF();
            float left = this.rects.position(i).x();
            float top = this.rects.position(i).y();
            float right = left + this.rects.position(i).width();
            float bot = top+this.rects.position(i).height();
            rect.set(left,top,right,bot);
            rects.add(rect);
        }
        this.rects=null;
        return rects;
    }

    //drawing of the view
    @Override
    protected void onDraw(Canvas canvas){


        ArrayList<RectF> foundRects = this.getRects();

        if (foundRects==null)return;
        float scaleX=(float)getWidth()/image.width();
        float scaleY=(float)getHeight()/image.height();
        float xShrink=1.3F;
        float yShrink=1.2F;

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(2);
        paint.setStyle(Paint.Style.STROKE);
        for (int i = 0; i<foundRects.size();i++){
            float left = foundRects.get(i).left*scaleX;
            left=left*xShrink;
            float top = foundRects.get(i).top*scaleY;
            top=top*yShrink;
            float right = left + foundRects.get(i).width()*scaleX;
            right=right/xShrink;
            float bot = top+foundRects.get(i).height()*scaleY;
            bot=bot/yShrink;
            foundRects.get(i).set(left,top,right,bot);
            canvas.drawRect(foundRects.get(i),paint);
            Log.e(TAG,"i = " + i);
            if (OpenCVTesting.debug) {
                Log.e(TAG, "Rect size: " + foundRects.get(i).width() + " x " + foundRects.get(i).height());
                Log.e(TAG, "new rect size: " + foundRects.get(i).width() + " x " + foundRects.get(i).height());
                Log.e(TAG, "canvas: " + getWidth() + " x " + getHeight());
            }
        }


        //if (foundRects.width()!=0 && rect.width()!=0)
       // Log.e(TAG, "old rect: " + foundRects.width()/foundRects.height() + " new: " + rect.width()/rect.height());

    }

}
