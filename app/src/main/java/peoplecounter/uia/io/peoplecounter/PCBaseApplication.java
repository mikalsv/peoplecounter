package peoplecounter.uia.io.peoplecounter;

import android.app.Application;
import android.content.Context;


/**
 * Singleton class for global application context
 */
public class PCBaseApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }


    /**
     *
     * @return Application Context
     */
    public static Context getContext() {
        return mContext;
    }
}
