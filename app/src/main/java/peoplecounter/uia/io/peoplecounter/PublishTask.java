package peoplecounter.uia.io.peoplecounter;

import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class LongOperation extends AsyncTask<String, Void, Void> {
   List<String> vars;
    public LongOperation(List<String> var) {
        this.vars = var;
    }

    @Override
    protected Void doInBackground(String... params) {
        Log.d("UP", vars.get(0));
        Log.d("DOWN", vars.get(1));

        execute(vars.get(0), vars.get(1));
        return null;
    }



    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}


    public void execute(String up, String down) {
        sendJson("http://uia.io:8181/post", up, down);
    }

    protected void sendJson( final String url,  final String up, final String down) {

                Looper.prepare(); //For Preparing Message Pool for the child Thread
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject json = new JSONObject();

                try {
                    HttpPost post = new HttpPost(url);
                    json.put("up", up);
                    json.put("down", down);
                    StringEntity se = new StringEntity( json.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    /*Checking response */
                    if(response!=null){
                        InputStream in = response.getEntity().getContent(); //Get the data in the entity
                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                Looper.loop(); //Loop in the message queue
            }




}