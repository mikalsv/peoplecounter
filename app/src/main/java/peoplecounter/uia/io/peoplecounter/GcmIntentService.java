package peoplecounter.uia.io.peoplecounter;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;


public class GcmIntentService extends IntentService{
    public static final String TAG = "GCM MSG HANDLER";


    // MSG CODES.
    public static String MSG_CONFIG = "1";


    public GcmIntentService() {
        super("422145115692");

        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO Auto-generated method stub
        Log.w(TAG,"Recieved an push-message from backend.");
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);


        if (!extras.isEmpty()) {

            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
             //   sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
            //    sendNotification("Deleted messages on server: " +
                        extras.toString();
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {


                    if(extras.getString("type").equals(MSG_CONFIG)) {
                        updateConfigMessage(Integer.parseInt(extras.getString("COUNT_LINE_Y")),
                                Integer.parseInt(extras.getString("COUNT_LINE_DELTA")),
                                Integer.parseInt(extras.getString("BLOB_MARGIN")),
                                Integer.parseInt(extras.getString("BLOB_MISSING_LIFETIME")),
                                Integer.parseInt(extras.getString("SENSITIVITY_VALUE")),
                                Integer.parseInt(extras.getString("BLUR_VALUE")));
                    }


            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private  void sendMessageToActivity() {
        Intent intent = new Intent("ConfigUpdate");
        // You can also include some extra data.
        Bundle b = new Bundle();
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void updateConfigMessage(int countline_y, int count_line_delta, int blob_margin, int blob_missing_lifetime, int sens_value, int blur_value) {

        Log.d(TAG, " New settings saved.");
        Log.d(TAG, countline_y + " " + count_line_delta + " " + blob_margin + " " + blob_missing_lifetime + " " + sens_value + " " +  blur_value);

        // Save preferences to SharedPreferences.
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getResources().getString(R.string.COUNT_LINE_DELTA),count_line_delta);
        editor.putInt(getResources().getString(R.string.COUNT_LINE_Y),countline_y);
        editor.putInt(getResources().getString(R.string.BLOB_MARGIN),blob_margin);
        editor.putInt(getResources().getString(R.string.BLOB_MISSING_LIFETIME),blob_missing_lifetime);
        editor.putInt(getResources().getString(R.string.SENSITIVITY_VALUE),sens_value);
        editor.putInt(getResources().getString(R.string.BLUR_VALUE),blur_value);

        editor.apply();
        editor.commit();

        doRestart(this);
    }



    public static void doRestart(Context c) {
        try {
            //check if the context is given
            if (c != null) {
                //fetch the packagemanager so we can get the default launch activity
                // (you can replace this intent with any other activity if you want
                PackageManager pm = c.getPackageManager();
                //check if we got the PackageManager
                if (pm != null) {
                    //create the intent with the default start activity for your application
                    Intent mStartActivity = pm.getLaunchIntentForPackage(
                            c.getPackageName()
                    );
                    if (mStartActivity != null) {
                        mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //create a pending intent so the application is restarted after System.exit(0) was called.
                        // We use an AlarmManager to call this intent in 100ms
                        int mPendingIntentId = 223344;
                        PendingIntent mPendingIntent = PendingIntent
                                .getActivity(c, mPendingIntentId, mStartActivity,
                                        PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        //kill the application
                        System.exit(0);
                    } else {
                        Log.e(TAG, "Was not able to restart application, mStartActivity null");
                    }
                } else {
                    Log.e(TAG, "Was not able to restart application, PM null");
                }
            } else {
                Log.e(TAG, "Was not able to restart application, Context null");
            }
        } catch (Exception ex) {
            Log.e(TAG, "Was not able to restart application");
        }
    }

}