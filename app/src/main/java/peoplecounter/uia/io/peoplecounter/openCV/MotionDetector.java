package peoplecounter.uia.io.peoplecounter.openCV;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.util.Log;
import android.view.View;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_imgproc;


import org.bytedeco.javacpp.opencv_objdetect;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

import peoplecounter.uia.io.peoplecounter.Settings;


public class MotionDetector extends View implements Camera.PreviewCallback {
    private static final String TAG = "MotionDetector";

 //   RectF motionRect;
    ArrayList<RectF>movingObjects=new ArrayList<>();
    ArrayList<RectF>humans=new ArrayList<>();

    private int cWidth;
    private int cHeight;
    private int outWidth;
    private int outHeight;
    private static final boolean debug=true;
    private int downSample;
    private final static int hz=60;
    private boolean processing=false;
    private Paint paint;
    opencv_core.IplImage differ;
    opencv_core.IplImage pDiffer;


    /*
     * Counting
     */


    private Paint linePaint;



    int upCount=0;

    public int getDownCount() {
        return downCount;
    }

    public void setDownCount(int downCount) {
        this.downCount = downCount;
    }

    int downCount=0;

    ArrayList<PendingBlob> pendingBlobs = new ArrayList<>();
    CounterLine counterLine;

    public int getUpCount() {
        return upCount;
    }

    public void setUpCount(int upCount) {
        this.upCount = upCount;
    }


    boolean calibrate=true;
    opencv_core.IplImage bgImage;
    opencv_core.IplImage image;
    opencv_core.IplImage image2;

    opencv_core.CvMemStorage storage;

    opencv_core.IplImage output;
    opencv_core.Rect rects;
    private boolean alternating=true;

    public MotionDetector(Context context, Camera camera){

        super(context);
        this.downSample=Settings.DOWN_SAMPLE_FACTOR;
    storage= opencv_core.CvMemStorage.create();
         paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setTextSize(50);

        linePaint = new Paint();
        linePaint.setColor(Color.GREEN);
        linePaint.setStrokeWidth(4);
        linePaint.setStyle(Paint.Style.STROKE);
        camera.setPreviewCallback(this);
        //Preload the opencv_objdetect module to work around a known bug.
        Loader.load(opencv_objdetect.class);
        Camera.Size prsize=camera.getParameters().getPreviewSize();
        outWidth=prsize.width;
        outHeight=prsize.height;
        cWidth=prsize.width/downSample;
        cHeight=prsize.height/downSample;
        image= opencv_core.IplImage.create(cWidth,cHeight,opencv_core.IPL_DEPTH_8U,1);
        image2= opencv_core.IplImage.create(cWidth,cHeight,opencv_core.IPL_DEPTH_8U,1);
        bgImage= opencv_core.IplImage.create(cWidth,cHeight,opencv_core.IPL_DEPTH_8U,1);

        float x1=0;
        float x2=prsize.width;


        counterLine=new CounterLine(x1,x2,Settings.COUNT_LINE_Y,Settings.COUNT_LINE_DELTA);

    }

    public void calibrate(){
        this.calibrate=true;
        Log.e(TAG,"Calibrating...");
    }

    private void processImage(byte[] data,int width, int height){
        opencv_core.IplImage image;
        if (calibrate){ image = bgImage;calibrate=false;}
      else  if (alternating) {
            image = this.image;
        }
        else image=this.image2;

        alternating=!alternating;
        opencv_core.Rect foundRects = new opencv_core.Rect();

        ByteBuffer imgbuffer =image.getByteBuffer();

        for (int y = 0;y<image.height();y++){
            for (int x = 0;x<image.width();x++){
                imgbuffer.put(x+y*image.widthStep(),data[(y*width*downSample)+downSample*x]);
            }
        }


    }

    private void process(){
opencv_core.cvClearMemStorage(storage);
         differ = null;


        differ = opencv_core.IplImage.create(image.width(), image.height(), image.depth(), 1);


        opencv_core.cvAbsDiff(image, image2, differ);

       opencv_imgproc.cvSmooth(differ,differ,opencv_imgproc.CV_BLUR,Settings.BLUR_VALUE,Settings.BLUR_VALUE,0,0);
        //threshold setting

        opencv_imgproc.cvThreshold(differ, differ, Settings.SENSITIVITY_VALUE, 255, opencv_imgproc.CV_THRESH_BINARY);

        setOutputImage(differ);
        getMovement(differ);
        getHumans();
        checkIntersect();
        checkoutBlobs();

        postInvalidate();
    }

    private void checkoutBlobs(){
        for (int i =0; i<pendingBlobs.size();i++){
            if (pendingBlobs.get(i).exiting && !pendingBlobs.get(i).exists){
                if (pendingBlobs.get(i).entering==PendingBlob.ENTERING_BOTTOM){
                    upCount++;
                }
                else downCount++;
                pendingBlobs.remove(i);
            }
            else if (!pendingBlobs.get(i).exists && !pendingBlobs.get(i).exiting){
                //the blob is no longer being tracked, but it also hasnt exited
                if (pendingBlobs.get(i).timeoutFrames>Settings.BLOB_MISSING_LIFETIME)pendingBlobs.remove(i);
                else pendingBlobs.get(i).markMissing();

            }
        }
    }

    private void checkIntersect(){
        evaluateBlobs();
        for (RectF rectf : humans){
            if (counterLine.interceptingBot(rectf) && counterLine.interceptingTop(rectf)){
                Log.e(TAG,"Intercepting both");
                PendingBlob blob = checkBlobExists(rectf,Settings.BLOB_MARGIN);
                if (blob==null){
                    //the blob doesnt exist, but it is intercepting both lines
                    Log.e(TAG,"Blob doesnt exist, but it is intercepting both lines");
                    continue;
                }
            }
            if  (counterLine.interceptingBot(rectf)){
                //the rectangle is intercepting the bottom line
                //if a PendingBlob already exists that occupies this width field
                //do nothing, if not then add a new PendingBlob
                Log.e(TAG,"Intercepting bot");
                PendingBlob blob = checkBlobExists(rectf,Settings.BLOB_MARGIN);
                if (blob==null){
                    //the blob didnt already exist, create it
                    Log.e(TAG,"Blob didnt exist, creating");
                    blob = new PendingBlob(rectf.left,rectf.right,PendingBlob.ENTERING_BOTTOM);
                    pendingBlobs.add(blob);
                }
                else{
                    Log.e(TAG,"Blob exists");
                    //the blob already existed, check if it should be marked as exiting, and update its position
                    blob.setPosition(rectf.left,rectf.right);
                    if (blob.entering==PendingBlob.ENTERING_TOP){
                        //yes, it is exiting the counting zone
                        Log.e(TAG,"Blob is exiting");
                        if (!blob.exiting) blob.exiting=true;

                    }

                }
            }
            if (counterLine.interceptingTop(rectf)){
                //the rectangle is intercepting the top line
                Log.e(TAG,"Intercepting top");
                PendingBlob blob = checkBlobExists(rectf,Settings.BLOB_MARGIN);
                if (blob==null){
                    //the blob didnt already exist, create it
                    Log.e(TAG,"Blob didnt exist, creating");
                    blob = new PendingBlob(rectf.left,rectf.right,PendingBlob.ENTERING_TOP);
                    pendingBlobs.add(blob);
                }
                else{
                    Log.e(TAG,"Blob exists");
                    //the blob already existed, check if it should be marked as exiting
                    blob.setPosition(rectf.left,rectf.right);
                    if (blob.entering==PendingBlob.ENTERING_BOTTOM){
                        //yes, it is exiting the counting zone
                        Log.e(TAG,"Blob is exiting");
                        if (!blob.exiting) blob.exiting=true;
                    }

                }
            }

        }
    }

    private void evaluateBlobs(){
        for (PendingBlob blob : pendingBlobs){
            blob.evaluate();
        }
    }


    private PendingBlob checkBlobExists(RectF rectf, int margin){
        for (PendingBlob blob : pendingBlobs){
            //if the smallest x2 is smaller than the biggest x1
            //they are not intercepting
            if (min(rectf.right,blob.x2)<max(rectf.left,blob.x1)){
                continue;
            }
            //however if that is not the case, they are
            //get the smallest x2, this is the right bound
            float right=(min(rectf.right,blob.x2));
            //get the biggest x1, this is the left bound
            float left=(max(rectf.left,blob.x1));

            float overlap = right-left;

            if (overlap> Settings.BLOB_MARGIN )return blob;

        }
        return null;
    }

    private float min(float a, float b){
        if (a<b)return a;
        else return b;
    }

    private float max(float a, float b){
        if (a>b) return a;
        else return b;
    }



    private void getMovement(opencv_core.IplImage _differ){
        movingObjects.clear();
        pDiffer =_differ.clone();
        //find contours
        opencv_core.CvSeq contour = new opencv_core.CvSeq(null);


        opencv_imgproc.cvFindContours(pDiffer, storage, contour, Loader.sizeof(opencv_core.CvContour.class), opencv_imgproc.CV_RETR_EXTERNAL, opencv_imgproc.CV_CHAIN_APPROX_SIMPLE);

        while (contour!=null && !contour.isNull()){
        opencv_core.CvBox2D box = opencv_imgproc.cvMinAreaRect2(contour, storage);
        opencv_core.CvPoint2D32f center = box.center();



        float left = center.x()*downSample-(box.size().height()*downSample)/2;
        float top = center.y()*downSample-(box.size().width()*downSample)/2;
        float right = center.x()*downSample+(box.size().height()*downSample)/2;
        float bot = center.y()*downSample+(box.size().width()*downSample)/2;
        movingObjects.add(new RectF(left,top,right,bot));
            contour=contour.h_next();
        }
    }

    private void getHumans(){
        humans.clear();

        for (RectF rectf : movingObjects)
        if ((rectf.height()<Settings.HUMAN_FILTER_LOWER || rectf.width()<Settings.HUMAN_FILTER_LOWER) || rectf.height()>Settings.HUMAN_FILTER_UPPER || rectf.width()>Settings.HUMAN_FILTER_UPPER  )
            continue;
        else humans.add(rectf);
    }


    private synchronized void setOutputImage(opencv_core.IplImage img){
        this.output=img;
    }

    private synchronized opencv_core.IplImage getOutputImage(){
        return new opencv_core.IplImage(this.output);
    }

    @Override
    public void onPreviewFrame(final byte[] data,final Camera camera) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                processing=true;
                long time1=System.currentTimeMillis();
                Camera.Size size = camera.getParameters().getPreviewSize();
                processImage(data,size.width,size.height);
                process();
                camera.addCallbackBuffer(data);
                long time2=System.currentTimeMillis();
                long delta=time2-time1;

        }});

            thread.start();

        }




    //drawing of the view
    @Override
    protected void onDraw(Canvas canvas) {


        if (output == null) return;
        opencv_core.IplImage image = getOutputImage();

        Bitmap map = Bitmap.createBitmap(image.width(), image.height(), Bitmap.Config.ALPHA_8);
        map.copyPixelsFromBuffer(image.getByteBuffer());
        Log.e(TAG, "Size of image:" + map.getWidth() + " x " + map.getHeight() + " -bytecount: " + map.getByteCount());
        map.prepareToDraw();

        //drawing does not take orientation into account apparently, flip width and height
        RectF rect = new RectF(cHeight,cWidth, 500, 500);
        Bitmap scaledmap=Bitmap.createScaledBitmap(map,outWidth,outHeight,true);
        canvas.drawBitmap(map, null, rect, paint);
        //if (motionRect!=null)
       // canvas.drawRect(motionRect,paint);


        if (humans!=null && humans.size()>0){
            for (RectF rectf : humans){
                canvas.drawRect(rectf,paint);

            }
        }
        canvas.drawText("UP: " + this.upCount,500,500,paint);
        canvas.drawText("DOWN: " + this.downCount,500,400,paint);

        canvas.drawLine(counterLine.x1,counterLine.yTop,counterLine.x2,counterLine.yTop,linePaint);
        canvas.drawLine(counterLine.x1,counterLine.yBot,counterLine.x2,counterLine.yBot,linePaint);



    }


}
