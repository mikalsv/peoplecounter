package peoplecounter.uia.io.peoplecounter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Settings {

    /**
     * The two count lines center position on screen
     * Changing this does not require application restart
     */
    public static float COUNT_LINE_Y;
    /**
     * The delta Y value of the two count lines
     * Changing this does not require application restart
     */
    public static float COUNT_LINE_DELTA;
    /**
     * The overlap margin required for two blobs to be considered the same
     * example: if BLOB_MARGIN=10, two blobs must overlap by 11 pixels to
     * be considered the same, if BLOB_MARGIN=0, they only need to overlap by 1
     * Changing this does not require application restart
     */
    public static int BLOB_MARGIN;
    /**
     * How many frames a vanished blob should last
     * Changing this does not require application restart
     */
    public static int BLOB_MISSING_LIFETIME;


    /**
     * The sensitivity value of the openCV threshold function
     * Changing this does not require application restart
     */
    public static int SENSITIVITY_VALUE;

    /**
     * The blur value of the openCV smoothing function
     * Changing this does not require application restart
     */
    public static int BLUR_VALUE;

    /**
     * The downscaling factor of the camera preview feed
     * Changing this requires application restart
     */
    public static int DOWN_SAMPLE_FACTOR = 8;

    public static float HUMAN_FILTER_LOWER = 150;
    public static  float HUMAN_FILTER_UPPER = 800;


    public  Settings (Context context) {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        COUNT_LINE_Y = preferences.getInt(context.getResources().getString(R.string.COUNT_LINE_Y),1);
        COUNT_LINE_DELTA = preferences.getInt(context.getResources().getString(R.string.COUNT_LINE_DELTA),400);
        BLOB_MARGIN = preferences.getInt(context.getResources().getString(R.string.BLOB_MARGIN),100);
        BLOB_MISSING_LIFETIME = preferences.getInt(context.getResources().getString(R.string.BLOB_MISSING_LIFETIME),10);
        SENSITIVITY_VALUE = preferences.getInt(context.getResources().getString(R.string.SENSITIVITY_VALUE),30);
        BLUR_VALUE = preferences.getInt(context.getResources().getString(R.string.BLUR_VALUE),20);

        Log.d("SETTINGS", "LOADED SETTINGS" + COUNT_LINE_Y + " " + COUNT_LINE_DELTA + " " + BLOB_MARGIN + " " + BLOB_MISSING_LIFETIME +  " " + SENSITIVITY_VALUE + " " + BLUR_VALUE);
    }
}
